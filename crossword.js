var row = 6;
var column = 7;

var characterPuzzle=[['A','I','M','*','D','E','N'],['*','M','E','*','O','N','E'],
                     ['U','P','O','N','*','T','O'],['S','O','*','E','R','I','N'],
                     ['*','S','A','*','O','R','*'],['I','E','S','*','D','E','A']];

function fillCrossword(){

  

  document.getElementById('0_0').innerHTML = characterPuzzle[0][0];
  document.getElementById('0_1').innerHTML = characterPuzzle[0][1]; 
  document.getElementById('0_2').innerHTML = characterPuzzle[0][2];
  document.getElementById('0_3').innerHTML = characterPuzzle[0][3];
  document.getElementById('0_4').innerHTML = characterPuzzle[0][4];
  document.getElementById('0_5').innerHTML = characterPuzzle[0][5];
  document.getElementById('0_6').innerHTML = characterPuzzle[0][6];
  document.getElementById('1_0').innerHTML = characterPuzzle[1][0];
  document.getElementById('1_1').innerHTML = characterPuzzle[1][1];
  document.getElementById('1_2').innerHTML = characterPuzzle[1][2];
  document.getElementById('1_3').innerHTML = characterPuzzle[1][3];
  document.getElementById('1_4').innerHTML = characterPuzzle[1][4];
  document.getElementById('1_5').innerHTML = characterPuzzle[1][5];
  document.getElementById('1_6').innerHTML = characterPuzzle[1][6];
  document.getElementById('2_0').innerHTML = characterPuzzle[2][0];
  document.getElementById('2_1').innerHTML = characterPuzzle[2][1];
  document.getElementById('2_2').innerHTML = characterPuzzle[2][2];
  document.getElementById('2_3').innerHTML = characterPuzzle[2][3];
  document.getElementById('2_4').innerHTML = characterPuzzle[2][4];
  document.getElementById('2_5').innerHTML = characterPuzzle[2][5];
  document.getElementById('2_6').innerHTML = characterPuzzle[2][6];
  document.getElementById('3_0').innerHTML = characterPuzzle[3][0];
  document.getElementById('3_1').innerHTML = characterPuzzle[3][1];
  document.getElementById('3_2').innerHTML = characterPuzzle[3][2];
  document.getElementById('3_3').innerHTML = characterPuzzle[3][3];
  document.getElementById('3_4').innerHTML = characterPuzzle[3][4];
  document.getElementById('3_5').innerHTML = characterPuzzle[3][5];
  document.getElementById('3_6').innerHTML = characterPuzzle[3][6];
  document.getElementById('4_0').innerHTML = characterPuzzle[4][0];
  document.getElementById('4_1').innerHTML = characterPuzzle[4][1];
  document.getElementById('4_2').innerHTML = characterPuzzle[4][2];
  document.getElementById('4_3').innerHTML = characterPuzzle[4][3];
  document.getElementById('4_4').innerHTML = characterPuzzle[4][4];
  document.getElementById('4_5').innerHTML = characterPuzzle[4][5];
  document.getElementById('4_6').innerHTML = characterPuzzle[4][6];
  document.getElementById('5_0').innerHTML = characterPuzzle[5][0];
  document.getElementById('5_1').innerHTML = characterPuzzle[5][1];
  document.getElementById('5_2').innerHTML = characterPuzzle[5][2];
  document.getElementById('5_3').innerHTML = characterPuzzle[5][3];
  document.getElementById('5_4').innerHTML = characterPuzzle[5][4];
  document.getElementById('5_5').innerHTML = characterPuzzle[5][5];
  document.getElementById('5_6').innerHTML = characterPuzzle[5][6];
  
}

var temp = getWordsAcross(characterPuzzle, getNumberedCrossword(characterPuzzle) )
var temp1 = getWordsDown(characterPuzzle, getNumberedCrossword(characterPuzzle) )

function displayAcross(){
  document.getElementById("across").innerHTML = "Across_Words";
  var result="";
  for(i=0;i<temp.length;i++){
      result +=temp[i]+ '<br />'; 
     // document.write("\n");
   // document.write(temp[i] + "<br>")
  }
  //document.write("\n"+result);
  document.getElementById("word_across").innerHTML = result ;

}
function displayDown(){
  document.getElementById("down").innerHTML = "Down_Words";
  var result="";
  for(i=0;i<temp1.length;i++){
      result +=  temp1[i]+ '<br />';
      
   // document.write(temp[i] + "<br>")
  }
  
  document.getElementById("word_down").innerHTML = result;

}
var puzzleNumber=1;
var numberedCrossword=getNumberedCrossword(characterPuzzle);

console.log("\n" + "puzzle #" + puzzleNumber++ + ":");

 getWordsAcross(characterPuzzle,numberedCrossword);
 getWordsDown(characterPuzzle,numberedCrossword);

function getNumberedCrossword(characterPuzzle){

 var numberedCrossword=[[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0],[0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0],[0,0,0,0,0,0,0]];
 var num=1;
 var i;
 var j;
 for(i=0;i<row;i++){
    for(j=0;j<column;j++){
       if((characterPuzzle[i][j]!='*')&& (i == 0 || j == 0 || characterPuzzle[i - 1][j] == '*' || (characterPuzzle[i][j - 1]) == '*')){
         numberedCrossword[i][j]=num++;
}
}
}
return numberedCrossword;
} 

function getWordsAcross(characterPuzzle,numberedCrossword){
 var acrosslist= [];
 var i;
 var j;
 var s="";
 var temp="";
 for(i=0;i<row;i++){
    for(j=0;j<column;j++){
       if(numberedCrossword[i][j]!=0){
         str=numberedCrossword[i][j]+".";
         while((j<characterPuzzle[i].length)&&(characterPuzzle[i][j]!='*')){
               str+=characterPuzzle[i][j];
               temp=str+'\n';
               j++;
               }
        acrosslist.push(temp);
}
}
}
      return acrosslist;
}

function getWordsDown(characterPuzzle,numberedCrossword){
  var downList = [];
 console.log("\n" + "Down");
 var j;
 var i;
 var s="";
 var temp1="";
 for(j=0;j<column;j++){
  for(i=0;i<row;i++){
     if(numberedCrossword[i][j]!=0){
       str=numberedCrossword[i][j]+".";
       while((i<row)&&(characterPuzzle[i][j]!='*')){
           str+=characterPuzzle[i][j];
           temp1=str+'\n';
            i++;
            }
       downList.push(temp1);
}
}
}
return downList;
}

